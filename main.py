from math import tan, pi, sqrt
from statistics import mean

def ols_check(coordinate: tuple, gradient: float, intercept: float=0):
  """find square vertical distance from regression line
  y = ax + c
  """
  return (coordinate[1] - ((gradient * coordinate[0]) + intercept))**2

# FACTOR = 2
MAX = 100

data = (
  [59, 57, 42, 25, 21, 43],
  [81, 87, 75, 79, 65, 99]
)

points = list(zip(*data))
centre = (mean(data[0]), mean(data[1]))

gradients = [(tan(pi*(a/MAX)) if a != 0 else 0) for a in range(0, MAX)]

out = []
for gradient in gradients:
  intercept = centre[1] - (gradient*centre[0])
  total = 0.0
  for point in points:
    total += sqrt(ols_check(point, gradient, intercept=intercept))
  out.append((total, gradient, intercept))

best = min(out,key=lambda tup: tup[0])

f = '{:.6f}'
print(f"y={f.format(best[1])}x+{f.format(best[2])}")