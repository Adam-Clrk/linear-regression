from statistics import mean
from math import sqrt

def pearson_correlation(xList: list, yList: list) -> float:
  """Calculate the pearson correlation coefficient for two associated lists
  1: strong positive linear correlation
  0: no linear correlation
  -1: strong negative linear correlation
  """

  xBar = mean(xList)
  yBar = mean(yList)
  zipped = zip(xList, yList)

  num = sum([(tup[0] - xBar)*(tup[1] - yBar) for tup in zipped])

  xPart = sum([(x-xBar)**2 for x in xList])
  yPart = sum([(y-yBar)**2 for y in yList])
  denom = sqrt(xPart) * sqrt(yPart)
  return num/denom

#print(pearson_correlation([59, 57, 42, 25, 21, 43], [81, 87, 75, 79, 65, 99]))
